package eu.inmite.android.fw.soap.test;


import eu.inmite.android.fw.soap.Ksoap2Marshaller;
import eu.inmite.android.fw.soap.SoapRequestBundle;
import eu.inmite.android.fw.soap.exception.SoapException;
import eu.inmite.android.fw.soap.test.model.SampleRequestPojo1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kxml2.kdom.Element;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * @author Tomáš Kypta
 * @since 05/05/2013
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest= Config.NONE)
public class TestKsoap2Marshaller {

	@Test
	public void testPrepareSoapHeaders() throws SoapException {
		SoapRequestBundle bundle = new SoapRequestBundle();
		bundle.setNamespace("somenamespace");
		SampleRequestPojo1 pojo1 = new SampleRequestPojo1();
		bundle.setRequestObject(pojo1);

		Ksoap2Marshaller marshaller = new Ksoap2Marshaller();
		Element[] elems = marshaller.prepareSoapHeaders(bundle);
		assertNotNull(elems);
	}
}
