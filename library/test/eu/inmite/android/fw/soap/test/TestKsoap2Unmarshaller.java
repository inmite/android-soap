package eu.inmite.android.fw.soap.test;

import eu.inmite.android.fw.soap.Ksoap2Unmarshaller;
import eu.inmite.android.fw.soap.exception.SoapException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ksoap2.serialization.SoapPrimitive;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * @author Tomáš Kypta
 * @since 15/05/2013
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest= Config.NONE)
public class TestKsoap2Unmarshaller {

	@Test
	public void testParseSoapPrimitive() throws SoapException {
		Ksoap2Unmarshaller unmarshaller = new Ksoap2Unmarshaller();

		SoapPrimitive soapPrimitive = new SoapPrimitive("testnamespace", "somekey", "val");
		String str = (String) unmarshaller.parseSoapResponse(soapPrimitive, String.class, request);
		Assert.assertNotNull(str);
		Assert.assertEquals("val", str);
	}
}
