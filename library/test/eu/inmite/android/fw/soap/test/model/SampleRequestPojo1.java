package eu.inmite.android.fw.soap.test.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Tomáš Kypta
 * @since 05/05/2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SampleRequestPojo1", propOrder = {
		"code",
		"name"
})
public class SampleRequestPojo1 {

	@XmlElement(required = true)
	protected String code;
	@XmlElement(required = true)
	protected String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
