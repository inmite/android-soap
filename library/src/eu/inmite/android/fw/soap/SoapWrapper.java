/*
 * Copyright (c) 2013, Inmite s.r.o. (www.inmite.eu).
 *
 * All rights reserved. This source code can be used only for purposes specified
 * by the given license contract signed by the rightful deputy of Inmite s.r.o.
 * This source code can be used only by the owner of the license.
 *
 * Any disputes arising in respect of this agreement (license) shall be brought
 * before the Municipal Court of Prague.
 */

package eu.inmite.android.fw.soap;


import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;

import java.util.List;
import java.util.Map;

import eu.inmite.android.fw.DebugLog;
import eu.inmite.android.fw.interfaces.IService;
import eu.inmite.android.fw.soap.exception.SoapException;

/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 3/5/13
 * Time: 12:54 PM
 */
public class SoapWrapper implements IService {

	private String mServerUrl;
	private SoapWrapperOptions mOptions;

	public SoapWrapper(String serverUrl) {
		this(serverUrl, new SoapWrapperOptions());
	}

	public SoapWrapper(String serverUrl, SoapWrapperOptions options) {
		mServerUrl = serverUrl;
		mOptions = options;
	}

	public Object sendRequest(SoapRequestBundle requestBundle) throws SoapException {
		return sendRequest(requestBundle, null, null);
	}

	public Object sendRequest(SoapRequestBundle requestBundle, Class responseClass) throws SoapException {
		return sendRequest(requestBundle, responseClass, null);
	}

	public Object sendRequest(SoapRequestBundle requestBundle, Class responseClass, SoapRequestBundle headerBundle) throws SoapException {
		return sendRequest(requestBundle, responseClass, headerBundle, null);
	}

	public Object sendRequest(SoapRequestBundle requestBundle, Class responseClass, SoapRequestBundle headerBundle, Map<String, String> httpHeaders) throws SoapException {
		if (requestBundle == null || requestBundle.getRequestObject() == null) {
			throw new SoapException("Cannot send null object");
		}

		String methodName = SoapWrapperUtil.getMethodName(requestBundle.getRequestObject());
		SoapObject request = new SoapObject(requestBundle.getNamespace(), methodName);

		Ksoap2Marshaller marshaller = new Ksoap2Marshaller();
		marshaller.prepareSoapRequest(request, requestBundle.getRequestObject());

		HttpTransportWrapper httpTransport = new HttpTransportWrapper(mServerUrl, mOptions.getConnectionTimeout());

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		// do not add any extra xml attributes
		envelope.setAddAdornments(false);
		envelope.implicitTypes = true;
		envelope.setOutputSoapObject(request);

		// add headers
		if (headerBundle != null) {
			envelope.headerOut = marshaller.prepareSoapHeaders(headerBundle);
		}

		String requestData = httpTransport.getSoapRequest(envelope);
		DebugLog.d("SoapWrapper request: " + requestData);
		if (mOptions.isDebugEnabled()) {
			httpTransport.debug = true;
		}

		String soapAction = null;
		List<HeaderProperty> headers = SoapWrapperUtil.prepareHttpHeaders(httpHeaders);
		try {
			httpTransport.call(soapAction, envelope, headers);
		} catch (Exception e) {
			DebugLog.w("SoapWrapper.sendRequest() failed to send SOAP request", e);
			throw new SoapException("Failed to send SOAP request", e);
		} finally {
			if (mOptions.isDebugEnabled()) {
				DebugLog.d("Soap request: " + httpTransport.requestDump);
				if (headers != null) {
					for (HeaderProperty hp : headers) {
						DebugLog.d("Soap request http header: " + hp.getKey() + ":" + hp.getValue());
					}
				}
			}
		}

		try {
			Object response = SoapWrapperUtil.getResponse(envelope.bodyIn);
			Ksoap2Unmarshaller unmarshaller = new Ksoap2Unmarshaller();
			return unmarshaller.parseSoapResponse(response, responseClass, request);
		} catch (SoapException e) {
			throw e;
		} catch (Exception e) {
			DebugLog.w("SoapWrapper failed to process SOAP response", e);
			throw new SoapException("Failed to process SOAP response", e);
		} finally {
			if (mOptions.isDebugEnabled()) {
				DebugLog.d("Soap response: " + httpTransport.responseDump);
			}
		}
	}



}
