package eu.inmite.android.fw.soap;

/**
 * @author Tomáš Kypta
 * @since 15/05/2013
 */
public class SoapWrapperOptions {

	private int mConnectionTimeout = 30;
	private boolean mDebugEnabled = false;

	public int getConnectionTimeout() {
		return mConnectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		mConnectionTimeout = connectionTimeout;
	}

	public boolean isDebugEnabled() {
		return mDebugEnabled;
	}

	public void setDebugEnabled(boolean debugEnabled) {
		mDebugEnabled = debugEnabled;
	}
}
