package eu.inmite.android.fw.soap.exception;

import org.ksoap2.SoapFault;

/**
 * @author Tomáš Kypta
 * @since 06/05/2013
 */
public class SoapException extends Exception {

	private SoapFault mSoapFault;

	public SoapException() {
	}

	public SoapException(String detailMessage) {
		super(detailMessage);
	}

	public SoapException(SoapFault soapFault, String detailMessage) {
		super(detailMessage);
		mSoapFault = soapFault;
	}

	public SoapException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public SoapException(Throwable throwable) {
		super(throwable);
	}

	public SoapFault getSoapFault() {
		return mSoapFault;
	}
}
