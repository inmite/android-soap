/*
 * Copyright (c) 2013, Inmite s.r.o. (www.inmite.eu).
 *
 * All rights reserved. This source code can be used only for purposes specified
 * by the given license contract signed by the rightful deputy of Inmite s.r.o.
 * This source code can be used only by the owner of the license.
 *
 * Any disputes arising in respect of this agreement (license) shall be brought
 * before the Municipal Court of Prague.
 */

package eu.inmite.android.fw.soap;

import eu.inmite.android.fw.DebugLog;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.IOException;
import java.net.Proxy;

/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 3/19/13
 * Time: 1:36 PM
 */
public class HttpTransportWrapper extends HttpTransportSE {

	public HttpTransportWrapper(String url) {
		super(url);
	}

	public HttpTransportWrapper(Proxy proxy, String url) {
		super(proxy, url);
	}

	public HttpTransportWrapper(String url, int timeout) {
		super(url, timeout);
	}

	public HttpTransportWrapper(Proxy proxy, String url, int timeout) {
		super(proxy, url, timeout);
	}

	public HttpTransportWrapper(String url, int timeout, int contentLength) {
		super(url, timeout, contentLength);
	}

	public HttpTransportWrapper(Proxy proxy, String url, int timeout, int contentLength) {
		super(proxy, url, timeout, contentLength);
	}

	public String getSoapRequest(SoapEnvelope envelope) {
		try {
			byte[] data = createRequestData(envelope, "UTF-8");
			return new String(data, "UTF-8");
		} catch (IOException e) {
			DebugLog.w("Failed to get soap request data");
		}
		return null;
	}
}
