package eu.inmite.android.fw.soap;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlSeeAlso;

import eu.inmite.android.fw.DebugLog;
import eu.inmite.android.fw.soap.exception.SoapException;

/**
 * @author Tomáš Kypta
 * @since 05/05/2013
 */
public class Ksoap2Unmarshaller {

	private Object parseSoapPrimitive(SoapPrimitive soapPrimitive, Class responseClass) throws SoapException {
		String soapValue = soapPrimitive.toString();
		try {
			if (String.class.equals(responseClass)) {
				return soapValue;
			} else if (BigDecimal.class.equals(responseClass)) {
				return new BigDecimal(soapValue);
			} else if (long.class.equals(responseClass)) {
				return Long.parseLong(soapValue);
			} else if (int.class.equals(responseClass)) {
				return Integer.parseInt(soapValue);
			} else if (boolean.class.equals(responseClass)) {
				return Boolean.parseBoolean(soapValue);
			} else if (responseClass.isEnum()) {
				try {
					return Enum.valueOf(responseClass, soapValue);
				} catch (IllegalArgumentException e) {

				}

				Field[] flds = responseClass.getDeclaredFields();
				HashMap<String, Field> hmValues = new HashMap<String, Field>();
				for (Field f : flds) {
					if (f.isEnumConstant()) {
						XmlEnumValue xmlEnumValue = (XmlEnumValue) f.getAnnotation(XmlEnumValue.class);
						if (xmlEnumValue != null) {
							hmValues.put(xmlEnumValue.value(), f);
						}
					}
				}

				Field f = hmValues.get(soapValue);
				if (f == null) {
					throw new IllegalArgumentException("Unknown enum value");
				}
				return Enum.valueOf(responseClass, f.getName());

//                ((Enum)responseClass).
//                Object[] enumConsts = responseClass.getEnumConstants();
//                for (Object ec : enumConsts) {
//                    if (((String)ec).equals(soapValue)) {
//                        return
//                    }
//                }
			}
		} catch (Exception e) {
			DebugLog.i("SoapWrapper.parseSoapPrimitive failed: " + e.getMessage());
			throw new SoapException("Failed to parse soap primitive");
		}
		return null;
	}

	public Object parseSoapResponse(Object response, Class responseClass, SoapObject request) throws SoapException {
		if (response instanceof SoapPrimitive) {
			return parseSoapPrimitive((SoapPrimitive) response, responseClass);
		}

		Object responseObject = null;
		try {
			if (responseClass != null) {
				responseObject = responseClass.newInstance();
			}
		} catch (InstantiationException e) {
			DebugLog.i("SoapWrapper.parseSoapResponse() - Failed to create response object: " + responseClass.getName());
			throw new SoapException("Failed to create response object");
		} catch (IllegalAccessException e) {
			DebugLog.i("SoapWrapper.parseSoapResponse() - Cannot create response object: " + responseClass.getName());
			throw new SoapException("Cannot to create response object");
		}

		if (response instanceof PropertyInfo) {
			PropertyInfo pi = (PropertyInfo) response;
			parseSoapResponseFromPropertyInfo(pi, responseObject, responseClass, request);
		} else if (response instanceof SoapObject) {
			// SoapObject might in some cases be difficult to parse properly, use PropertyInfos instead
			Object responseAdjusted = SoapWrapperUtil.getResponse(response);
			return parseSoapResponse(responseAdjusted, responseClass, request);
//            SoapObject soapResponse = (SoapObject) response;
//            Field fields[] = responseClass.getDeclaredFields();
//            try {
//                for (Field field : fields) {
//                    if (soapResponse.hasProperty(field.getName())) {
//                        Object responseRawItem = soapResponse.getProperty(field.getName());
//                        Object responseItem = parseSoapResponse(responseRawItem, field.getType());
//                        field.setAccessible(true);
//                        field.set(responseObject, responseItem);
//                    }
//                }
//            } catch (IllegalAccessException e) {
//                throw new SoapException("Cannot set response field");
//            } catch (IllegalArgumentException e) {
//                throw new SoapException("Failed to parse soap response");
//            }
		} else if (response instanceof Vector) {
			Vector<Object> vec = (Vector<Object>) response;
			DebugLog.d("Response: " + vec.size());

			// detect response class according first vector
			String responseTypeName = (String)vec.get(0);
			String packageName = SoapWrapperUtil.nameSpace2packageName(request.getNamespace());

			if (responseClass == null) {
				// try to directly instantiate target object
				try {
					responseClass = Class.forName(packageName + "." + responseTypeName);
				} catch (ClassNotFoundException e) {
					DebugLog.v("Try to instantiate " + responseTypeName + " failed, try to use ObjectFactory");
				}
			}

			if (responseClass == null) {
				// try to use ObjectFactory for build response type
				try {
					Class<?> clazz = Class.forName(packageName + "." + "ObjectFactory");
					for (Method method : clazz.getMethods()) {
						if (method.isAnnotationPresent(XmlElementDecl.class)) {
							XmlElementDecl a = method.getAnnotation(XmlElementDecl.class);
							if (request.getNamespace().equals(a.namespace()) && responseTypeName.equals(a.name())) {
								Object o = clazz.newInstance();
								JAXBElement<?> responseElement = (JAXBElement<?>) method.invoke(o, new Object[]{null});
								responseClass = responseElement.getDeclaredType();
								break;
							}
						}
					}
				} catch (ClassNotFoundException e) {
					DebugLog.e("ObjectFactory class was not found in package: " + packageName + " it is not possible to create response object");
				} catch (Exception e) {
					DebugLog.e("Search for response type from ObjectFactory failed", e);
				}
			}

			XmlSeeAlso xmlSeeAlso = (XmlSeeAlso) responseClass.getAnnotation(XmlSeeAlso.class);
			if (xmlSeeAlso != null) {
				for (Class sa : xmlSeeAlso.value()) {
					if (sa.getSimpleName().equals((String) vec.get(0))) {
						responseClass = sa;
						break;
					}
				}
			}

			try {
				responseObject = responseClass.newInstance();

				for (int i = 1; i < vec.size(); i++) {
					PropertyInfo pi = (PropertyInfo) vec.get(i);
					parseSoapResponseFromPropertyInfo(pi, responseObject, responseClass, request);
				}
			} catch (Exception e) {
				DebugLog.e("Failet to create instance of " + responseClass.toString(), e);
			}
		}
		return responseObject;
	}


	private void parseSoapResponseFromPropertyInfo(PropertyInfo pi, Object responseObject, Class responseClass, SoapObject request) throws SoapException {
		try {
			List<Field> fields = SoapWrapperUtil.getAllFields(new ArrayList<Field>(), responseClass);
			Field field = null;
			for (Field f : fields) {
				XmlElement xmlElement = (XmlElement) f.getAnnotation(XmlElement.class);
				if (xmlElement != null) {
					if (xmlElement.name().equals(pi.getName())) {
						field = f;
						break;
					}
				}
			}
			for (Field f : fields) {
				if (f.getName().equals(pi.getName())) {
					field = f;
					break;
				}
			}
//            if (field == null) {
//                field = responseClass.getDeclaredField(pi.getName());
//            }

			boolean handled = false;
//            Field field = responseClass.getDeclaredField(pi.getName());
			if (List.class.isAssignableFrom(field.getType())) {
				Class argumentClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
				field.setAccessible(true);
				ArrayList list = (ArrayList) field.get(responseObject);
				if (list == null) {
					list = new ArrayList();
				}
				Object responseItem = parseSoapResponse(pi.getValue(), argumentClass, request);
				list.add(responseItem);
				field.set(responseObject, list);
				handled = true;
//            } else if (!field.getType().isPrimitive() && Modifier.isAbstract( field.getType().getModifiers() )) {
			} else {
				Class type = field.getType();
				XmlSeeAlso xmlSeeAlso = (XmlSeeAlso) field.getType().getAnnotation(XmlSeeAlso.class);
				if (xmlSeeAlso != null) {
					for (Class sa : xmlSeeAlso.value()) {
						String returnedName = null;
						Object val = pi.getValue();
						if (val instanceof SoapObject) {
							returnedName = ((SoapObject) val).getName();
						} else if (val instanceof SoapPrimitive) {
							returnedName = ((SoapPrimitive) val).getName();
						}
						if (sa.getSimpleName().equals(returnedName)) {
							type = sa;
							break;
						}
					}
				}

				Object responseItem = parseSoapResponse(pi.getValue(), type, request);
				field.setAccessible(true);
				field.set(responseObject, responseItem);
			}
//        } catch (NoSuchFieldException e) {
//            throw new SoapException("No such field: " + pi.getName());
		} catch (IllegalAccessException e) {
			throw new SoapException("Cannot set response field");
		} catch (IllegalArgumentException e) {
			throw new SoapException("Failed to parse soap response");
		}
	}

}
