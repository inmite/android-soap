package eu.inmite.android.fw.soap;

/**
 * @author Tomáš Kypta
 * @since 05/05/2013
 */
public class SoapRequestBundle {

	private Object requestObject;
	private String namespace;

	public SoapRequestBundle() {}

	public SoapRequestBundle(Object requestObject, String namespace) {
		this.requestObject = requestObject;
		this.namespace = namespace;
	}

	public Object getRequestObject() {
		return requestObject;
	}

	public void setRequestObject(Object requestObject) {
		this.requestObject = requestObject;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
}
