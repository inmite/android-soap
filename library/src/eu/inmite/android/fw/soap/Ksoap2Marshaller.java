package eu.inmite.android.fw.soap;

import eu.inmite.android.fw.soap.exception.SoapException;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tomáš Kypta
 * @since 05/05/2013
 */
public class Ksoap2Marshaller {

	public Element[] prepareSoapHeaders(SoapRequestBundle... requestObjects) throws SoapException {
		if (requestObjects.length == 0) {
			return null;
		}

		Element[] elements = new Element[requestObjects.length];
		for (int i = 0; i < requestObjects.length; i++) {
			SoapRequestBundle bundle = requestObjects[i];
			elements[i] = prepareSoapHeader(bundle.getRequestObject(), bundle.getNamespace());
		}
		return elements;
	}


	private Element prepareSoapHeader(Object requestHeaderObject, String headerNamespace) throws SoapException {
		if (requestHeaderObject == null) {
			return null;
		}
		Element element = new Element();
		XmlType typeAnnot = (XmlType) requestHeaderObject.getClass().getAnnotation(XmlType.class);
		if (typeAnnot != null) {
			element.setName(typeAnnot.name());
		} else {
			element.setName(requestHeaderObject.getClass().getSimpleName());
		}
		element.setNamespace(headerNamespace);

		List<Field> fields = SoapWrapperUtil.getAllFields(new ArrayList<Field>(), requestHeaderObject.getClass());
//        Field[] fields = requestHeaderObject.getClass().getDeclaredFields();
		try {
			for (Field field : fields) {
				field.setAccessible(true);
				Object fieldValue = field.get(requestHeaderObject);
				String fieldName = field.getName();

				XmlElement xmlElement = (XmlElement) field.getAnnotation(XmlElement.class);
				if (xmlElement != null) {
					if (!xmlElement.name().equals("##default")) {
						fieldName = xmlElement.name();
					}
//                    if (xmlElement.required()) {
//                        if (fieldValue == null) {
//                            throw new SoapException("Request header field " + field.getName() + " cannot be null");
//                        }
//                    }
				}

				if (field.getType().equals(String.class)) {

				} else if (field.getType().equals(BigDecimal.class)) {

				} else if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {

				} else if (field.getType().equals(Double.class) || field.getType().equals(double.class)) {

				} else {
					// complex nested objects
					if (fieldValue != null) {
						Element innerElement = prepareSoapHeader(fieldValue, "");
						innerElement.setName(fieldName);
						element.addChild(Node.ELEMENT, innerElement);
					} else {
						Element innerElement = new Element();
						innerElement.setName(fieldName);
						innerElement.setNamespace("");
						element.addChild(Node.ELEMENT, innerElement);
					}

					continue;
				}

//                if (fieldValue != null) { //  TODO for testing signature problems
				Element innerElement = new Element();
				innerElement.setName(fieldName);
				innerElement.setNamespace("");
				if (fieldValue != null) {
					innerElement.addChild(Node.TEXT, fieldValue.toString());
				}
				element.addChild(Node.ELEMENT, innerElement);
//                }

			}

			return element;
		} catch (IllegalAccessException e) {
			throw new SoapException("Cannot add request properties");
		} catch (IllegalArgumentException e) {
			throw new SoapException("Failed to prepare soap request");
		}
	}




	public static void prepareSoapRequest(SoapObject request, Object requestObject) throws SoapException {
		if (requestObject == null) {
			throw new SoapException("Cannot process null object");
		}

		List<Field> fields = SoapWrapperUtil.getAllFields(new ArrayList<Field>(), requestObject.getClass());
		try {
			for (Field field : fields) {
				field.setAccessible(true);
				Object fieldValue = field.get(requestObject);
				String fieldName = field.getName();

				XmlElement xmlElement = field.getAnnotation(XmlElement.class);
				if (xmlElement != null) {
					if (!xmlElement.name().equals("##default")) {
						fieldName = xmlElement.name();
					}
					if (xmlElement.required()) {
						if (fieldValue == null) {
							throw new SoapException("Request field " + field.getName() + " cannot be null");
						}
					}
				}

				PropertyInfo propInfo = new PropertyInfo();
				propInfo.name = fieldName;

				if (field.getType().equals(String.class)) {
					propInfo.type = PropertyInfo.STRING_CLASS;
				} else if (field.getType().equals(BigDecimal.class)) {
					propInfo.type = BigDecimal.class;
				} else if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
					propInfo.type = PropertyInfo.INTEGER_CLASS;
				} else if (field.getType().equals(Long.class) || field.getType().equals(long.class)) {
					propInfo.type = PropertyInfo.BOOLEAN_CLASS;
				} else if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class)) {
					propInfo.type = PropertyInfo.LONG_CLASS;
				} else if (field.getType().equals(Double.class) || field.getType().equals(double.class)) {
					propInfo.type = Double.class;
				} else if (field.getType().equals(Float.class) || field.getType().equals(float.class)) {
					propInfo.type = Float.class;
				} else {
					// complex nested objects
					if (fieldValue != null) {
						propInfo.type = PropertyInfo.OBJECT_CLASS;
						SoapObject nestedObject = new SoapObject("", fieldName);
						prepareSoapRequest(nestedObject, fieldValue);

						request.addSoapObject(nestedObject);
					}
//                    else {
////                        prepareSoapRequestFields(nestedObject, field.getType());
//                    }
					continue;
				}
				if (fieldValue != null) {
					request.addPropertyIfValue(propInfo, fieldValue);
				}
			}
		} catch (IllegalAccessException e) {
			throw new SoapException("Cannot add request properties");
		} catch (IllegalArgumentException e) {
			throw new SoapException("Failed to prepare soap request");
		}
	}

	private static void prepareSoapRequestFields(SoapObject request, Class requestClass) throws SoapException {
		List<Field> fields = SoapWrapperUtil.getAllFields(new ArrayList<Field>(), requestClass);
		try {
			for (Field field : fields) {
				PropertyInfo propInfo = new PropertyInfo();
				propInfo.name = field.getName();

				if (field.getType().equals(String.class)) {
					propInfo.type = PropertyInfo.STRING_CLASS;
				} else if (field.getType().equals(BigDecimal.class)) {
					propInfo.type = new BigDecimal(0).getClass();
				} else {
					// complex nested objects
					propInfo.type = PropertyInfo.OBJECT_CLASS;
					SoapObject nestedObject = new SoapObject("", field.getName());
//                    prepareSoapRequestFields(nestedObject, field.getType());
					request.addSoapObject(nestedObject);
					return;
				}
				request.addPropertyIfValue(propInfo, null);
			}
		} catch (IllegalArgumentException e) {
			throw new SoapException("Failed to prepare soap request");
		}
	}
}
