package eu.inmite.android.fw.soap;

import android.net.Uri;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import eu.inmite.android.fw.soap.exception.SoapException;

/**
 * @author Tomáš Kypta
 * @since 15/05/2013
 */
public class SoapWrapperUtil {

	public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
		if (type.getSuperclass() != null) {
			fields = getAllFields(fields, type.getSuperclass());
		}

		Field[] entityFields = type.getDeclaredFields();
		XmlType xmlType = (XmlType) type.getAnnotation(XmlType.class);

		if (xmlType != null) {
			// order by propOrder
			String[] propOrder = xmlType.propOrder();

			Map<String, Field> fieldsByName = new HashMap<String, Field>();
			for (int i = 0; i < entityFields.length ; i++) {
				fieldsByName.put(entityFields[i].getName(), entityFields[i]);
			}

			for (String fieldName : propOrder) {
				fields.add(fieldsByName.get(fieldName));
			}

		} else {
			// no order
			for (Field field : entityFields) {
				fields.add(field);
			}
		}

		return fields;
	}

	public static String getMethodName(Object requestObject) throws SoapException {
		if (requestObject == null) {
			throw new SoapException("Cannot process null object");
		}

		XmlRootElement rootElement = (XmlRootElement) requestObject.getClass().getAnnotation(XmlRootElement.class);
		if (rootElement != null) {
			return rootElement.name();
		}
		// fallback to class name
		return requestObject.getClass().getSimpleName();
	}

	public static List<HeaderProperty> prepareHttpHeaders(Map<String, String> httpHeaders) {
		List<HeaderProperty> list = new ArrayList<HeaderProperty>();
		if (httpHeaders != null) {
			for (Map.Entry<String, String> entry : httpHeaders.entrySet()) {
				list.add(new HeaderProperty(entry.getKey(), entry.getValue()));
			}
		}
		return list;
	}


	public static Object getResponse(Object bodyIn) throws SoapException {
		if (bodyIn instanceof SoapFault) {
			throw new SoapException((SoapFault) bodyIn, "Soap body contain error message");
		}
		KvmSerializable ks = (KvmSerializable) bodyIn;

		if (ks.getPropertyCount() == 0) {
			return null;
		} else if (ks.getPropertyCount() == 1) {
			return getProperty(ks, 0);
		} else {
			Vector ret = new Vector();
			ret.add(((SoapObject) ks).getName());
			for (int i = 0; i < ks.getPropertyCount(); i++) {
				ret.add(getProperty(ks, i));
			}
			return ret;
		}
	}

	private static Object getProperty(KvmSerializable ks, int index) {
		PropertyInfo pi = new PropertyInfo();
		ks.getPropertyInfo(index, null, pi);
		return pi;
	}

	public static String nameSpace2packageName(String namespace) {
		Uri uri = Uri.parse(namespace);
		String[] hostp = uri.getHost().split("\\.");
		StringBuilder sb = new StringBuilder();
		for (int i = hostp.length-1; i >= 0; i--) {
			sb.append(hostp[i]);
			sb.append(".");
		}
		sb.deleteCharAt(sb.length()-1);
		if (uri.getPath().length() > 1) {
			sb.append(uri.getPath().toLowerCase(Locale.US).replace("/", "."));
		}
		return sb.toString();
	}

}
