/*
 * Copyright (c) 2013, Inmite s.r.o. (www.inmite.eu).
 *
 * All rights reserved. This source code can be used only for purposes specified
 * by the given license contract signed by the rightful deputy of Inmite s.r.o.
 * This source code can be used only by the owner of the license.
 *
 * Any disputes arising in respect of this agreement (license) shall be brought
 * before the Municipal Court of Prague.
 */

package javax.xml.bind.annotation;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 3/6/13
 * Time: 4:39 PM
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.PACKAGE, ElementType.TYPE})
public @interface XmlAccessorType {
	XmlAccessType value() default XmlAccessType.PUBLIC_MEMBER;
}
