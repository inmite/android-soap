# android-soap library

Android library for SOAP API utilizing [android-ksoap2](https://code.google.com/p/ksoap2-android/).

Note:

  - The library is currently under development and lacks many features (e.g. polymorphism).
  - The library uses JAXB annotations generated on POJO classes generated with [Apache CXF](http://cxf.apache.org/docs/maven-cxf-codegen-plugin-wsdl-to-java.html).
  - The library uses Java Reflection API to bind/unbind Java POJO classes to android-ksoap2 request/response data structures.

## How to include it in your project:

	<dependency>
		<groupId>eu.inmite.android.lib</groupId>
		<artifactId>android-soap</artifactId>
		<version>(insert latest version)</version>
		<type>jar</type>
	</dependency>

## How to use the library

Create an instance of `SoapWrapper`. Then use its methods `sendRequest()` to send requests via SOAP.
All requests are instances of `SoapRequestBundle` - contains object and namespace.
